package com.itb.dailytales.model

data class Register(
    val email: String,
    val username: String,
    val password: String
)
