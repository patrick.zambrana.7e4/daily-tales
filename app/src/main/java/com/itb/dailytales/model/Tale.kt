package com.itb.dailytales.model

import java.util.*

data class Tale (
    val id: Int,
    val profile_id : Int,
    var title: String,
    var text: String,
    var created_at: Date? = null
)