package com.itb.dailytales.model

import java.util.*

data class ReplyResponse(
    val id: Long? = null,
    val comment_id: Int,
    val profile_id: Profile,
    var content: String,
    var created_at: Date? = null
)
