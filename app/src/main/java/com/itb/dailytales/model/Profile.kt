package com.itb.dailytales.model

data class Profile(
    val id: Int,
    val user: String,
    var profile_name: String,
    var signature: String,
    var description: String,
    var image: String,
    val followers: Int,
    val following: Int
)