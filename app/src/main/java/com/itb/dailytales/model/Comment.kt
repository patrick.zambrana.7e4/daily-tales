package com.itb.dailytales.model

import java.util.*


data class Comment(
    val id: Long? = null,
    val tale_id: Int,
    val profile_id: Int,
    var content: String,
    var replies: MutableList<Reply>? = null,
//    var likes: Int,
    var created_at: Date? = null
)
//{
//    var replies = mutableListOf<Comment>()

//    override fun toString(): String {
//        return "Comment(id=$id, profile=$profile_id, content='$content', likes=$likes, timestamp=$created_at, replies=$replies)"
//    }


//}