package com.itb.dailytales.model

data class LoginResponse(
    val email: String,
    val username: String,
    val password: String
)
