package com.itb.dailytales.model

import java.util.*

data class Reply(
    val id: Long? = null,
    val comment_id: Int,
    val profile_id: Int,
    var content: String,
    var created_at: Date? = null
)
