package com.itb.dailytales.model

data class Login(
    val email: String,
    val password: String
)