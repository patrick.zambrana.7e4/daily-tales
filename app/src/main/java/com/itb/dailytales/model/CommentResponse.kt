package com.itb.dailytales.model

import java.util.*

data class CommentResponse(
    val id: Long? = null,
    val tale_id: Int,
    val profile_id: Profile,
    var content: String,
    var replies: MutableList<ReplyResponse>? = null,
    var created_at: Date? = null
)