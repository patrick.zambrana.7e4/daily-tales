package com.itb.dailytales.model

data class User (
    val id: Int,
    val name: String,
    val email: String,
    val created_at: String
//    var password: String,
//    var signature: String,
//    var profileImage: Int // PROVISIONAL (DRAWABLE)
)