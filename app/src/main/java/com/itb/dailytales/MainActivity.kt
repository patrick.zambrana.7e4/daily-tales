package com.itb.dailytales

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import androidx.room.Room
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.itb.dailytales.database.UserDatabase
import com.itb.dailytales.databinding.ActivityMainBinding
import com.itb.dailytales.ui.view.CreatorActivity
import com.itb.dailytales.ui.view.LoginActivity
import com.itb.dailytales.ui.viewModel.TalesViewModel

class MainActivity : AppCompatActivity() {

    companion object {
        lateinit var database: UserDatabase
    }

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: TalesViewModel
    var actualFragmentId = R.id.homeFragment // Updates when user switches between fragments
    private var isLogged: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {

        loadUserData() // Load credentials

        if (!isLogged) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
        // Remove TopBar: DO NOT MOVE DOWN. It wont work.
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        supportActionBar?.hide()
        super.onCreate(savedInstanceState)

        // database init
        database = Room.databaseBuilder(this,
            UserDatabase::class.java,"UserDatabase").build()

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // INIT VIEW MODELS
        viewModel = ViewModelProvider(this).get(TalesViewModel::class.java)
        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_main)
        val appBarConfiguration = AppBarConfiguration(setOf(
            R.id.homeFragment, R.id.profileFragment))
        //setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        navView.setOnItemSelectedListener {
            when(it.itemId) {
                R.id.creatorFragment -> {
                    startActivity(Intent(this, CreatorActivity::class.java))
                    overridePendingTransition(R.anim.from_bottom, R.anim.to_top)
                    navView.selectedItemId = actualFragmentId
                }
                R.id.profileFragment -> {
                    navController.navigate(R.id.action_homeFragment_to_profileFragment)
                    actualFragmentId = R.id.profileFragment
                }
                R.id.homeFragment -> {
                    navController.navigate(R.id.action_profileFragment_to_homeFragment)
                    actualFragmentId = R.id.homeFragment
                }
            }
            return@setOnItemSelectedListener true
        }

        navView.setOnNavigationItemReselectedListener { return@setOnNavigationItemReselectedListener }
    }

    override fun onResume() {
        super.onResume()
        binding.navView.menu.findItem(actualFragmentId).setChecked(true)
    }

    private fun loadUserData() {
        val sharedPreferences = getSharedPreferences("UserCredentials", Context.MODE_PRIVATE)
        val username = sharedPreferences.getString("EMAIL", null)
        val password = sharedPreferences.getString("PASSWORD", null)

        if (username != null && password != null)
            isLogged = true
    }
}