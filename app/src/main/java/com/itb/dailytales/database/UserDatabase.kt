package com.itb.dailytales.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.itb.dailytales.database.entity.UserEntity

@Database(entities = arrayOf(UserEntity::class), version = 1)
abstract class UserDatabase: RoomDatabase() {
    abstract fun userDao():UserDao
}