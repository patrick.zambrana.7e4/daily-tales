package com.itb.dailytales.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class UserEntity(
    @PrimaryKey(autoGenerate = true) var id:Long = 0,
    var userName:String,
    var email:String
    )
