package com.itb.dailytales.database

import androidx.room.*
import com.itb.dailytales.database.entity.UserEntity

@Dao
interface UserDao {
    @Query("select * from UserEntity")
    fun getAllUsers(): List<UserEntity>
    @Query("select * from UserEntity where userName = :userName ")
    fun getUserByName(userName:String): UserEntity
    @Insert
    fun addUser(UserEntity: UserEntity)
    @Update
    fun updateUser(UserEntity:UserEntity)
    @Delete
    fun deleteUser(UserEntity: UserEntity)
}