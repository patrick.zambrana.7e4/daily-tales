package com.itb.dailytales.repository

import com.itb.dailytales.MainActivity
import com.itb.dailytales.database.entity.UserEntity
import com.itb.dailytales.model.*
import com.itb.dailytales.network.ApiInterface
import retrofit2.Response

class MainRepository {
    private val api:ApiInterface

    companion object {
        fun getInstance(): MainRepository {
            val repository = MainRepository()
            return repository
        }
    }

    init {
        api = ApiInterface.getInstance()
    }

    // api
    suspend fun getAllUser(): Response<List<User>> { return api.getAllUsers() }
    suspend fun getTalesByProfileId(): Response<List<Tale>> { return api.getAllTales() }
    suspend fun postTale(tale: Tale): Response<Tale> {return api.postTale(tale)}
    suspend fun putTale(tale:Tale): Response<Tale> { return api.putTale(tale,tale.id)}
    suspend fun getTalesByProfileId(id:Int): Response<List<Tale>> { return api.getTalesByProfileId(id) }
    suspend fun getAllProfiles(): Response<List<Profile>> { return api.getAllProfiles()}
    suspend fun getProfileByUsername(userName:String): Response<List<Profile>> { return api.getProfileByUserName(userName)}
    suspend fun getProfileById(id:Int): Response<List<Profile>> { return api.getProfileById(id)}
    suspend fun putProfile(profile:Profile): Response<Profile> { return api.putProfile(profile,profile.id)}
    suspend fun postProfile(profile: Profile):Response<Profile>{return api.postProfile(profile)}
    suspend fun getCommentsByTaleId(taleId: Int): Response<List<CommentResponse>> { return api.getCommentsByTaleId(taleId)}
    suspend fun putComment(comment:Comment): Response<Comment> { return api.putComment(comment,comment.id!!.toInt())}
    suspend fun getRepliesByCommentId(commentId: Int): Response<List<ReplyResponse>> { return api.getRepliesByCommentId(commentId)}
    suspend fun putReply(reply:Reply): Response<Reply> { return api.putReply(reply,reply.id!!.toInt())}

    suspend fun addComment(comment: Comment): Response<CommentResponse> { return  api.postComment(comment)}
    suspend fun addReply(reply: Reply): Response<ReplyResponse> { return  api.postReply(reply)}

    suspend fun login(email: String, password: String ): Response<LoginResponse> {
        val login = Login(email, password)
        return api.postLogin(login)
    }

    suspend fun register(email: String, username: String, password: String ): Response<Register> {
        val register = Register(email, username,  password)
        return api.postRegister(register)
    }

    suspend fun logout(): Response<Void>{return api.postLogout(" ")}
    // database
    /**
     * return the last user inserted in room database
     */
    fun getLocalUser(): UserEntity? {
        val users:List<UserEntity> = MainActivity.database.userDao().getAllUsers()
        if( users.size != 0 )
            return users[users.size - 1]
        else
            return null
    }

    fun updateLocalUser(userEntity:UserEntity) {
        userEntity.id  = 0
        MainActivity.database.userDao().updateUser(userEntity)
    }
    fun addLocalUser(userEntity:UserEntity) {
        MainActivity.database.userDao().addUser(userEntity)
    }
}