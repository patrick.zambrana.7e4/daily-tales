package com.itb.dailytales.ui.viewModel

import android.graphics.Bitmap
import android.util.Base64
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.itb.dailytales.MainActivity
import com.itb.dailytales.database.entity.UserEntity
import com.itb.dailytales.model.*
import com.itb.dailytales.repository.MainRepository
import kotlinx.coroutines.*
import org.json.JSONObject
import org.json.JSONTokener
import java.io.ByteArrayOutputStream
import java.io.OutputStreamWriter
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class UserViewModel: ViewModel() {
    // vars
    val repository = MainRepository.getInstance()
    var liveUserList = MutableLiveData<List<User>>()
    var liveProfileList = MutableLiveData<List<Profile>>()
    var liveLocalUser =  MutableLiveData<UserEntity?>()
    var liveLocalProfile = MutableLiveData<Profile?>()
    var liveProfile = MutableLiveData<Profile?>()
    var liveImgLink = MutableLiveData<String>()

    // IMGUR
    private val CLIENT_ID = "3d6351c75d652d2"
    // methods
    fun getAllUsers(): MutableLiveData<List<User>> {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.getAllUser()}
            if( response.isSuccessful ) {
                liveUserList.postValue(response.body())
            } else {
                Log.e("GET-ALL-USERS", "No se han podido cargar los usuarios")
            }
        }
        return liveUserList
    }

    fun getAllProfiles() {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.getAllProfiles()}
            if( response.isSuccessful ) {
                liveProfileList.postValue(response.body())
            } else {
                Toast.makeText(coroutineContext as MainActivity, "No se han podido cargar los usuarios",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun getProfileByUsername(username: String): MutableLiveData<Profile?>  {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.getProfileByUsername(username)}
            if( response.isSuccessful ) {
                if(response.body()?.isEmpty() == true) {
                    Log.i(
                        "GET-PROFILE-USERNAME",
                        response.message() + "but there is no such Profile"
                    )
                    liveLocalProfile.value = null
                }
                else liveLocalProfile.postValue(response.body()!![0])
            } else {
                Log.i("GET-PROFILE-USERNAME",response.message())
            }
        }
        return liveLocalProfile
    }

    fun getProfileById(id: Int): MutableLiveData<Profile?>  {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.getProfileById(id)}
            if( response.isSuccessful ) {
                liveProfile.postValue(response.body()!![0])
                Log.i("GET-PROFILE-BY-ID", response.message())
            } else {
                Log.i("GET-PROFILE-BY-ID", response.message())
            }
        }
        return liveProfile
    }

    fun updateProfile(profile:Profile,onSuccess: () -> Unit?){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.putProfile(profile)}
            if( response.isSuccessful ) {
                Log.i("PUT-PROFILE","profile updated succesfully")
                onSuccess()
            }
            else {
                Log.i("PUT-PROFILE",response.body().toString())
                Log.i("PUT-PROFILE:error -> ",response.message())
            }
        }
    }

    fun updateProfile(profile:Profile){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.putProfile(profile)}
            if( response.isSuccessful ) {
                Log.i("PUT-PROFILE","profile updated succesfully")
            }
            else {
                Log.i("PUT-PROFILE",response.body().toString())
                Log.i("PUT-PROFILE:error -> ",response.message())
            }
        }
    }

    fun postProfile(profile: Profile, onSuccess: () -> Unit): MutableLiveData<Profile?> {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.postProfile(profile)}
            if( response.isSuccessful ) {
                Log.i("POST-PROFILE","Profile added succesfully")
                liveLocalProfile.postValue( response.body())
                onSuccess()
            }
            else {
                Log.i("POST-PROFILE",response.body().toString())
                Log.i("POST-PROFILE:error -> ",response.message())
            }
        }
        return liveLocalProfile
    }

    /**
     * Make a request with user data for logging in the app
     */
    fun login(email: String, password: String, onSuccess: (response: LoginResponse) -> Unit, onFailure: () -> Unit) {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.login(email, password)}
            if( response.isSuccessful ) {
                onSuccess(response.body()!!)
            }
            else {
                Log.i("logVm",response.body().toString())
                onFailure()
            }
        }
    }

    fun register(email: String, username: String, password: String, onSuccess: (response: Register) -> Unit, onFailure: (response: String) -> Unit) {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.register(email, username, password)}
            if( response.isSuccessful ) {
                onSuccess(response.body()!!)
            }
            else {
                Log.i("REGISTER",response.message())
                onFailure(response.message())
            }
        }
    }

    fun logout(onSuccess: () -> Unit){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.logout()}
            if( response.isSuccessful ) {
                Log.i("LOGOUT",response.message())
                onSuccess()
            }
            else {
                Log.i("LOGOUT",response.message())
            }
        }
    }

    // EXTRA: poder guardar varios usuarios a la vez
    fun getLocalUser(): MutableLiveData<UserEntity?> {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.getLocalUser() }
            liveLocalUser.postValue(response)
        }
        return liveLocalUser
    }

    fun addLocalUser(userEntity: UserEntity){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.addLocalUser(userEntity) }
        }
    }

    fun postImgurAndGetLink(image: Bitmap):MutableLiveData<String> {
        println("UPLOAD IMAGE")
        getBase64Image(image, complete = { base64Image ->
            GlobalScope.launch(Dispatchers.Default) {
                val url = URL("https://api.imgur.com/3/image")

                val boundary = "Boundary-${System.currentTimeMillis()}"

                val httpsURLConnection =
                    withContext(Dispatchers.IO) { url.openConnection() as HttpsURLConnection }
                httpsURLConnection.setRequestProperty("Authorization", "Client-ID $CLIENT_ID")
                httpsURLConnection.setRequestProperty(
                    "Content-Type",
                    "multipart/form-data; boundary=$boundary"
                )

                httpsURLConnection.requestMethod = "POST"
                httpsURLConnection.doInput = true
                httpsURLConnection.doOutput = true

                var body = ""
                body += "--$boundary\r\n"
                body += "Content-Disposition:form-data; name=\"image\""
                body += "\r\n\r\n$base64Image\r\n"
                body += "--$boundary--\r\n"


                val outputStreamWriter = OutputStreamWriter(httpsURLConnection.outputStream)
                withContext(Dispatchers.IO) {
                    outputStreamWriter.write(body)
                    outputStreamWriter.flush()
                }

                val response = httpsURLConnection.inputStream.bufferedReader()
                    .use { it.readText() }  // defaults to UTF-8
                val jsonObject = JSONTokener(response).nextValue() as JSONObject
                val data = jsonObject.getJSONObject("data")

                Log.d("DEBUG", "Link is : ${data.getString("link")}")
                liveImgLink.postValue(data.getString("link"))
            }
        })
        return liveImgLink
    }

//    @OptIn(DelicateCoroutinesApi::class)
    private fun getBase64Image(image: Bitmap, complete: (String) -> Unit) {
        GlobalScope.launch {
            println("BASE64")
            val outputStream = ByteArrayOutputStream()
            image.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
            val b = outputStream.toByteArray()
            complete(Base64.encodeToString(b, Base64.DEFAULT))
        }
    }
}