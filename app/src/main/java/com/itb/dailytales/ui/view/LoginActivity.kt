package com.itb.dailytales.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import com.itb.dailytales.R
import com.itb.dailytales.databinding.ActivityLoginBinding
import com.itb.dailytales.ui.adapter.LoginAdapter
import com.google.android.material.tabs.TabLayout

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        supportActionBar?.hide()

        setContentView(R.layout.activity_login)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.tabLayoutLogin.addTab(binding.tabLayoutLogin.newTab().setText("Iniciar Sesión"))
        binding.tabLayoutLogin.addTab(binding.tabLayoutLogin.newTab().setText("Registrarse"))
        binding.tabLayoutLogin.tabGravity = TabLayout.GRAVITY_FILL

        val adapter = LoginAdapter(supportFragmentManager)
        binding.viewPagerLogin.adapter = adapter
        binding.tabLayoutLogin.setupWithViewPager(binding.viewPagerLogin)

        // Animation Settings:
        binding.tabLayoutLogin.translationY = 300F
        binding.tabLayoutLogin.alpha = 0F

        // Starting animation
        binding.tabLayoutLogin.animate().translationY(0F).alpha(1F).setDuration(1000).setStartDelay(100).start()
    }
}