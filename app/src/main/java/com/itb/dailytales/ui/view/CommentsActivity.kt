package com.itb.dailytales.ui.view

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.Window
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.itb.dailytales.R
import com.itb.dailytales.databinding.ActivityCommentsBinding
import com.itb.dailytales.model.Comment
import com.itb.dailytales.model.Reply
import com.itb.dailytales.ui.adapter.CommentsAdapter
import com.itb.dailytales.ui.viewModel.CommentsViewModel
import com.itb.dailytales.ui.viewModel.CommentsViewModel.ReplyTo
import com.itb.dailytales.ui.viewModel.UserViewModel

class CommentsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCommentsBinding
    lateinit var commentVm: CommentsViewModel
    val userVm : UserViewModel by viewModels()
    private lateinit var adapter: CommentsAdapter
    private var taleId: Int? = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.CustomBackground)
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        supportActionBar?.hide()
        binding = ActivityCommentsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        commentVm = ViewModelProvider(this)[CommentsViewModel::class.java]
        val args = intent.extras
        taleId = args?.getInt("tale_id");
        if( userVm.liveLocalProfile.value == null) {
            val username = loadUserNameCredentials()
            userVm.getProfileByUsername(username)
        }
        adapter = CommentsAdapter()
        adapter.userVm = userVm
        adapter.commentVm = commentVm
        binding.commentsRecyclerView.layoutManager = LinearLayoutManager(this)
        binding.commentsRecyclerView.adapter = adapter

        commentVm.getCommentsByTaleId(taleId!!).observe(this) {
            adapter.setTaleComments(it)
        }
        ReplyTo.isReplying.observe(this) {
            if (it) {
                openKeyboard()
                binding.commentText.hint = ("Responder a " + ReplyTo.userToReply.profile_name)
            }
        }

        // Send comment when user presses Enter key.
        binding.commentText.setOnKeyListener { _, i, keyEvent ->
            when {
                keyEvent.action != KeyEvent.ACTION_DOWN -> false
                i == KeyEvent.KEYCODE_ENTER -> {
                    sendComment()
                    true
                }
                else -> false
            }
        }

        binding.sendCommentButton.setOnClickListener {
            sendComment()
        }
        binding.backButton.setOnClickListener {
            finish()
            overridePendingTransition(R.anim.from_left, R.anim.to_right) // ANIMATION
        }
    }

    private fun sendComment() {
        if (binding.commentText.text.toString().isNotBlank() && ReplyTo.isReplying.value == false) {
//            val comment = getCommentText()
//            viewModel.sendComment(comment)
            Log.i("comment",taleId!!.toString() + " " +userVm.liveLocalProfile.value!!.id )
            val newComment = Comment(0,taleId!!,userVm.liveLocalProfile.value!!.id,binding.commentText.text.toString()) // puede dar mal por no agrgar created_at
            commentVm.addComment(newComment)

            binding.commentText.setText("")
            hideKeyboard()
//            Log.i("TEST", "${comment.id}")
        } else if(ReplyTo.isReplying.value == true) {
//            adapter.addCommentReply(ReplyTo.commentToReply.id, getCommentText())
            val newReply = Reply(0,
                ReplyTo.commentToReply.id!!.toInt(),
                userVm.liveLocalProfile.value!!.id,
                binding.commentText.text.toString()) // puede dar mal por no agrgar created_at
            commentVm.addReply(newReply)
            ReplyTo.isReplying.postValue(false)
            binding.commentText.setText("")
            binding.commentText.hint = "Añadir comentario..."
            hideKeyboard()
        } else {
            binding.sendCommentButton.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake))
        }

        Log.i("TEST", "${ReplyTo.comments.value}")
        Log.i("TEST", "${ReplyTo.isReplying.value}")
    }

//    fun getCommentText() : Comment {
//        val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
//        return Comment(adapter.comments.size.toLong(), commentVm.testUser, binding.commentText.text.toString(), 0, sdf.calendar.time)
//    }

    fun hideKeyboard() {
        val imm: InputMethodManager = getSystemService(
            Context.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        imm.hideSoftInputFromWindow(binding.commentText.windowToken, 0)
    }

    fun openKeyboard() {
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    override fun onDestroy() {
        super.onDestroy()
        overridePendingTransition(R.anim.from_left, R.anim.to_right) // ANIMATION
    }

    private fun loadUserNameCredentials(): String {
        val sharedPreferences =
            getSharedPreferences("UserCredentials", Context.MODE_PRIVATE)
        return sharedPreferences?.getString("EMAIL", null)!!
    }

}