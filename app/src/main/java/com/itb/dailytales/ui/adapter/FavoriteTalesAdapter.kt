package com.itb.dailytales.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.RecyclerView
import com.itb.dailytales.R
import com.itb.dailytales.model.Tale

class FavoriteTalesAdapter(val viewModel: ViewModel) : RecyclerView.Adapter<FavoriteTalesAdapter.TalesViewHolder>() {
    var favTales = mutableListOf<Tale>()

    fun setTaleList(taleList: List<Tale>){
        favTales = taleList.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteTalesAdapter.TalesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.tale_layout, parent, false)
        return TalesViewHolder(view)
    }

    override fun onBindViewHolder(holder: FavoriteTalesAdapter.TalesViewHolder, position: Int) {
        holder.bindData(favTales[position])
    }

    override fun getItemCount(): Int {
        return favTales.size
    }

    inner class TalesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        private var profileImg = itemView.findViewById<ImageView>(R.id.item_profile_image)
        private var taleTitle: TextView = itemView.findViewById(R.id.item_tale_title)

        fun bindData(tale: Tale) {
            taleTitle.text = tale.title
        }
    }
}