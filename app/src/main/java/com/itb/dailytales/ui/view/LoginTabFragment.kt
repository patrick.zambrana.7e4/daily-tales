package com.itb.dailytales.ui.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.itb.dailytales.MainActivity
import com.itb.dailytales.databinding.LoginTabFragmentBinding
import com.itb.dailytales.model.LoginResponse
import com.itb.dailytales.ui.viewModel.UserViewModel
import java.util.*

class LoginTabFragment : Fragment() {
    private val vm : UserViewModel by activityViewModels()
    private var _binding: LoginTabFragmentBinding? = null
    private val binding get() = _binding!!
    private val SHARED_PREFERENCES_FILE = "UserCredentials"
    private var lastClickTime: Long = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = LoginTabFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Animation Settings:
        binding.loginSetUsername.translationX = 800F
        binding.loginSetPassword.translationX = 800F
        binding.loginForgotPassword.translationX = 800F
        binding.loginButton.translationX = 800F

        binding.loginSetUsername.alpha = 0F
        binding.loginSetPassword.alpha = 0F
        binding.loginForgotPassword.alpha = 0F
        binding.loginButton.alpha = 0F

        // Start Animation
        binding.loginSetUsername.animate().translationX(0F).alpha(1F).setDuration(800).setStartDelay(300).start()
        binding.loginSetPassword.animate().translationX(0F).alpha(1F).setDuration(800).setStartDelay(500).start()
        binding.loginForgotPassword.animate().translationX(0F).alpha(1F).setDuration(800).setStartDelay(500).start()
        binding.loginButton.animate().translationX(0F).alpha(1F).setDuration(800).setStartDelay(700).start()

        binding.loginButton.setOnClickListener {
            // preventing double, using threshold of 2000 ms
            if (SystemClock.elapsedRealtime() - lastClickTime > 2000){
                val email = binding.loginSetUsername.text.toString()
                val password = binding.loginSetPassword.text.toString()

                fun onSuccess(response: LoginResponse) {
                    saveCredentials(response.username, password)
                    startActivity(Intent(context, MainActivity::class.java))
                    requireActivity().finish()
                }

                fun onFailure() {
                    Toast.makeText(context, "Hay problemas con el email o password ingresados...",Toast.LENGTH_SHORT).show()
                }
                vm.login(email, password, ::onSuccess, ::onFailure)
            }
            lastClickTime = SystemClock.elapsedRealtime();

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun saveCredentials(email: String, password: String) {
        val sharedPref = activity?.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE)
        val editor = sharedPref!!.edit()
        editor.apply {
            putString("EMAIL", email)
            putString("PASSWORD", password)
        }.apply()

        Toast.makeText(context, "Data saved.", Toast.LENGTH_SHORT).show()
    }
}