package com.itb.dailytales.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.itb.dailytales.databinding.FragmentFavoritesBinding
import com.itb.dailytales.ui.adapter.CreatedTalesAdapter
import com.itb.dailytales.ui.viewModel.TalesViewModel

class FavoritesFragment : Fragment() {
    private val viewModel : TalesViewModel by activityViewModels()
    private var _binding: FragmentFavoritesBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentFavoritesBinding.inflate(inflater, container, false)
        val root: View = binding.root
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = CreatedTalesAdapter(viewModel)
        binding.favoriteTalesRecyclerview.layoutManager = LinearLayoutManager(this.requireContext())
        binding.favoriteTalesRecyclerview.adapter = adapter

        TalesViewModel.liveTaleList.observe(viewLifecycleOwner) {
            adapter.setTaleList(it)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}