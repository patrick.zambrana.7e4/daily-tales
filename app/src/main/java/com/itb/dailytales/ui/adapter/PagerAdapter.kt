package com.itb.dailytales.ui.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.itb.dailytales.ui.view.FavoritesFragment
import com.itb.dailytales.ui.view.ProfileTalesFragment

class PagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {return ProfileTalesFragment()}
            1 -> {return  FavoritesFragment()}
            else -> {return ProfileTalesFragment()}
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> {return "C U E N T O S"}
            1 -> {return "F A V O R I T O S"}
        }
        return super.getPageTitle(position)
    }

    override fun getCount(): Int {
        return 2
    }
}