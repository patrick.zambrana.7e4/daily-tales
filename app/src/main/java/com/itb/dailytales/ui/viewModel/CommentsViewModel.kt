package com.itb.dailytales.ui.viewModel

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.itb.dailytales.MainActivity
import com.itb.dailytales.model.*
import com.itb.dailytales.repository.MainRepository
import com.itb.dailytales.ui.viewModel.CommentsViewModel.ReplyTo.comments
import com.itb.dailytales.ui.viewModel.CommentsViewModel.ReplyTo.replies
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class CommentsViewModel : ViewModel() {

    private val repository = MainRepository.getInstance()
    // TEST USER
    var testUser: Profile = Profile(0, "admin", "irinalum@gmail.com", "irinamoon",
        "1234",  "R.drawable.profile_photo",0,0)

    object ReplyTo {
        var comments = MutableLiveData<MutableList<CommentResponse>>()
        var replies = MutableLiveData<MutableList<MutableList<Reply>>>()
        var userToReply : Profile = Profile(0,"", "", "", "", "", 0,0)
        var isReplying = MutableLiveData<Boolean>()
        var commentToReply: CommentResponse = CommentResponse(0, 1,
            Profile(0,"perdido","perdido","per","d","s",0,0),
            "just do it", null,Date())
    }

    init {
        var commentList = mutableListOf<CommentResponse>()
        comments.postValue(commentList)
        ReplyTo.isReplying.postValue(false)
    }

//    fun sendComment(comment: Comment) {
//        var list = comments.value
//        list?.add(comment)
//        comments.postValue(list!!)
//    }

    fun getCommentsByTaleId(taleId: Int): MutableLiveData<MutableList<CommentResponse>> {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.getCommentsByTaleId(taleId)}
            if( response.isSuccessful ) {
                comments.postValue(response.body()!! as MutableList<CommentResponse>)
            } else {
                Toast.makeText(coroutineContext as MainActivity, "No se han podido cargar los usuarios",
                    Toast.LENGTH_SHORT).show()
            }
        }
        return comments
    }

    fun getRepliesByCommentId(commentId: Int): MutableLiveData<MutableList<MutableList<Reply>>> {
        var aux: MutableList<MutableList<Reply>>? = null
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.getRepliesByCommentId(commentId)}
            if( response.isSuccessful ) {
                aux = replies.value
                aux?.add(response.body()!! as MutableList<Reply>)
                replies.postValue(aux)
            } else {
                Toast.makeText(coroutineContext as MainActivity, "No se han podido cargar los usuarios",
                    Toast.LENGTH_SHORT).show()
            }
        }
        return replies
    }

    fun addComment(comment: Comment) {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.addComment(comment)}
            if( response.isSuccessful ) {
                val updater = comments.value
                updater?.add(response.body()!!)
                comments.postValue(updater)
            }
            else {
                Log.i("commentVm",response.body().toString())
                Log.i("commentVm:error -> ",response.message())
            }
        }
    }

    fun addReply(reply: Reply) {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.addReply(reply)}
//            if( response.isSuccessful ) {
//                var aux = replies.value
//                aux?.add(response.body()!!)
//                val updater = replies.value
//                updater?.add(response.body()!!)
//                replies.postValue(updater)
//            }
//            else {
//                Log.i("commentVm",response.body().toString())
//            }
        }
    }



}