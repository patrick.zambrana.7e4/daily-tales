package com.itb.dailytales.ui.view

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.itb.dailytales.R
import com.itb.dailytales.databinding.FragmentEditableProfileBinding
import com.itb.dailytales.model.Profile
import com.itb.dailytales.ui.viewModel.UserViewModel
import com.squareup.picasso.Picasso


class EditableProfileFragment : Fragment() {
    companion object{
        val CREATE = 1
        val UPDATE = 0
        val DEFAULT_IMAGE ="https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/Ardea_cinerea_-_Pak_Thale.jpg/1280px-Ardea_cinerea_-_Pak_Thale.jpg"
    }
    private val vm : UserViewModel by activityViewModels()
    private var _binding: FragmentEditableProfileBinding? = null
    lateinit var toggle: ActionBarDrawerToggle
    private var imageUri: Uri? = null
    lateinit var imagePicker: ImagePicker
    var imageLink:String? = null

    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentEditableProfileBinding.inflate(inflater, container, false)
        val root: View = binding.root
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val actionType = arguments?.getInt("action_type")
        // load user
        val username = loadUserNameCredentials()
        vm.getProfileByUsername(username).observe(viewLifecycleOwner) { profile ->
            if( profile != null ) setProfileViewContent(profile)
        }

        //Config Profile
        toggle = ActionBarDrawerToggle(requireActivity(), binding.drawerLayout, R.string.open, R.string.close)
        binding.drawerLayout.addDrawerListener(toggle)
        binding.drawerLayout.setScrimColor(resources.getColor(R.color.transparent))
        toggle.syncState()

        (requireActivity() as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // listeners
        binding.profileImg.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                imagePicker.askPermissions()
            } else {
                val intent = Intent(Intent.ACTION_GET_CONTENT)
                intent.type = "image/*"
                startActivityForResult(intent, 100)
            }
        }
        binding.btnEditableApply.setOnClickListener {
            var newProfile: Profile?
            if(actionType == UPDATE )
                newProfile = vm.liveLocalProfile.value
            else
                newProfile = Profile(0,username,"","","","",0,0)

            newProfile!!.description = binding.description.text.toString()
            if(imageLink != null )
                newProfile.image = imageLink!!
            else if(actionType == CREATE)
                newProfile.image = DEFAULT_IMAGE

            newProfile.profile_name = binding.userName.text.toString()
            newProfile.signature = binding.signture.text.toString()
            // update or create profile ( local and api )

            fun navToProfileFragment(){
                // todo implementar una espera de unos cuantos milisegundos hasta que cargue el nuevo perfil
                // nav to profile
                val action = EditableProfileFragmentDirections.actionEditableProfileFragmentToProfileFragment()
                findNavController().navigate(action)
            }
            if(actionType == UPDATE ) {
                vm.liveLocalProfile.postValue(newProfile)
                vm.updateProfile(newProfile,::navToProfileFragment)
            } else {
                vm.postProfile(newProfile,::navToProfileFragment)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item))
            return true

        return super.onOptionsItemSelected(item)
    }

    private fun setProfileViewContent(profile: Profile) {
        binding.userName.setText(profile.profile_name)
        binding.signture.setText(profile.signature)
        binding.description.setText(profile.description)
        binding.followers.setText(profile.followers.toString().plus(resources.getString(R.string.seguidores)))
        binding.following.setText(profile.following.toString().plus(resources.getString(R.string.following)))
        Picasso.get().load(profile.image).into(binding.profileImg)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun loadUserNameCredentials(): String {
        val sharedPreferences =
            activity?.getSharedPreferences("UserCredentials", Context.MODE_PRIVATE)
        return sharedPreferences?.getString("EMAIL", null)!!
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // CHANGE CODE FOR GET DIFFERENT REQUESTS
        if (requestCode == 100) { // Upload From Gallery
            imageUri = data?.data!!
            //binding.imageTaken.setImageURI(imageUri) // handle chosen image
            Picasso.get().load(imageUri).centerCrop().fit().into(binding.profileImg)

            val bitmap = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                ImageDecoder.decodeBitmap(ImageDecoder.createSource(requireContext().contentResolver, imageUri!!))
            } else {
                MediaStore.Images.Media.getBitmap(requireContext().contentResolver, imageUri)
            }
            val imgLink = vm.postImgurAndGetLink(bitmap)
            imgLink.observe(viewLifecycleOwner){imageLink ->
                this.imageLink = imageLink
            }
        } else if (requestCode == 200) { // From Camera
            var bitmap: Bitmap? = (data?.extras?.get("data") as Bitmap?)
            imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context?.contentResolver, bitmap, "ImageTitle", null))
            if (bitmap != null) {
                binding.profileImg.setImageBitmap(bitmap)
                val imgLink = vm.postImgurAndGetLink(bitmap)
                imgLink.observe(viewLifecycleOwner){imageLink ->
                    this.imageLink = imageLink
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        imagePicker.onRequestPermissionsResult(requestCode, grantResults)
    }
}