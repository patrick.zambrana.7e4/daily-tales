package com.itb.dailytales.ui.view

import android.content.Intent
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import com.itb.dailytales.R
import com.itb.dailytales.databinding.FragmentPagerTaleBinding
import com.itb.dailytales.ui.viewModel.TalesViewModel
import com.itb.dailytales.ui.viewModel.UserViewModel
import com.like.LikeButton
import com.like.OnLikeListener

class TaleSlidePagerFragment : Fragment() {

    private val viewModel: TalesViewModel by viewModels()
    private val vmUser: UserViewModel by viewModels()
    private var _binding: FragmentPagerTaleBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentPagerTaleBinding.inflate(inflater, container, false)
        val root: View = binding.root
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // TALE DATA
        binding.taleTitle.text = viewModel.testTale.title
        binding.taleText.text = viewModel.testTale.text
        vmUser.getProfileById(viewModel.testTale.profile_id).observe(viewLifecycleOwner) {
                profile -> binding.taleSignature.text = profile?.signature ?: ""
        }
        binding.taleText.movementMethod = ScrollingMovementMethod()

        // BUTTONS
        var shake = AnimationUtils.loadAnimation(context, R.anim.shake) // Shake animation

        binding.commentButton.setOnClickListener {
            it.startAnimation(shake)
            NavHostFragment.findNavController(this).navigate(R.id.action_homeFragment_to_commentsActivity) // Navigate to comment section
        }
        binding.shareButton.setOnClickListener {
            it.startAnimation(shake)
            share()
        }
        binding.favButton.setOnLikeListener(object : OnLikeListener {
            override fun liked(likeButton: LikeButton) {
                //TODO
            }
            override fun unLiked(likeButton: LikeButton) {
                //TODO
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun share() {
        var intent = Intent(Intent.ACTION_SEND)
        intent.setType("text/plain")
        var shareBody = "Tale URL"
        var shareSub = binding.taleTitle.text
        intent.putExtra(Intent.EXTRA_SUBJECT, shareSub)
        intent.putExtra(Intent.EXTRA_TEXT, shareBody)
        startActivity(Intent.createChooser(intent, "Enviar a"))
    }
}