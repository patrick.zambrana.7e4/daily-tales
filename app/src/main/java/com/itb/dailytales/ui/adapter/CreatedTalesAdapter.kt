package com.itb.dailytales.ui.adapter

import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.RecyclerView
import com.itb.dailytales.R
import com.itb.dailytales.model.Tale

class CreatedTalesAdapter(val viewModel: ViewModel) : RecyclerView.Adapter<CreatedTalesAdapter.TalesViewHolder>(){
    var tales = mutableListOf<Tale>()

    fun setTaleList(taleList: List<Tale>){
        tales = taleList.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TalesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.tale_layout, parent, false)
        return TalesViewHolder(view)
    }

    override fun onBindViewHolder(holder: TalesViewHolder, position: Int) {
        holder.bindData(tales[position])
    }

    override fun getItemCount(): Int {
        return tales.size
    }

    inner class TalesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        private var profileImg = itemView.findViewById<ImageView>(R.id.item_profile_image)
        private var taleTitle: TextView = itemView.findViewById(R.id.item_tale_title)
        private var editTaleBtn: ImageButton = itemView.findViewById(R.id.edit_button)

        fun bindData(tale: Tale) {
            taleTitle.text = tale.title

            // Edit Tale Menu
            editTaleBtn.setOnClickListener {
                showPopupMenu(it)
            }
        }

        /**
         * Takes the button where menu will be displayed and sets the instructions for each item in menu.
         */
        private fun showPopupMenu(view: View) {
            val popup = PopupMenu(itemView.context, view)
            popup.inflate(R.menu.edit_tale_menu)

            popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item: MenuItem? ->
                when (item!!.itemId) {
                    R.id.edit_tale_option -> Toast.makeText(itemView.context, item.title, Toast.LENGTH_SHORT).show()
                    R.id.delete_tale_option -> Toast.makeText(itemView.context, item.title, Toast.LENGTH_SHORT).show()
                }
                true
            })

            popup.show()
        }
    }
}