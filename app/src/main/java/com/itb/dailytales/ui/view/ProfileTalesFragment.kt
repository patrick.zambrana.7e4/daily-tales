package com.itb.dailytales.ui.view

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.itb.dailytales.databinding.FragmentProfileTalesBinding
import com.itb.dailytales.ui.adapter.CreatedTalesAdapter
import com.itb.dailytales.ui.viewModel.TalesViewModel
import com.itb.dailytales.ui.viewModel.UserViewModel

class ProfileTalesFragment : Fragment() {
    private val viewModel : TalesViewModel by activityViewModels()
    private val userVm : UserViewModel by activityViewModels()
    private var _binding: FragmentProfileTalesBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentProfileTalesBinding.inflate(inflater, container, false)
        val root: View = binding.root
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = CreatedTalesAdapter(viewModel)
        binding.createdTalesRecyclerview.layoutManager = LinearLayoutManager(this.requireContext())
        binding.createdTalesRecyclerview.adapter = adapter
        //adapter.setTaleList(viewModel.taleList)
        userVm.liveLocalProfile.observe(viewLifecycleOwner) { profile ->
            if(profile != null)
                viewModel.getTalesByUser(profile!!.id).observe(viewLifecycleOwner) { tales ->
                    adapter.setTaleList(tales)
                }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}