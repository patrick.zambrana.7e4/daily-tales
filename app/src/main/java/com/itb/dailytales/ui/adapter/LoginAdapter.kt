package com.itb.dailytales.ui.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.itb.dailytales.ui.view.LoginTabFragment
import com.itb.dailytales.ui.view.RegisterTabFragment

class LoginAdapter(manager: FragmentManager): FragmentPagerAdapter(manager) {

    private var totalTabs = 2

    override fun getCount(): Int = totalTabs

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return LoginTabFragment()
            1 -> return RegisterTabFragment()
            else -> return LoginTabFragment()
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> {return "Iniciar sesión"}
            1 -> {return "Registrarse"}
        }
        return super.getPageTitle(position)
    }
}