package com.itb.dailytales.ui.view

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.itb.dailytales.R
import com.itb.dailytales.database.entity.UserEntity
import com.itb.dailytales.databinding.FragmentProfileBinding
import com.itb.dailytales.model.Profile
import com.itb.dailytales.ui.adapter.PagerAdapter
import com.itb.dailytales.ui.viewModel.UserViewModel
import com.squareup.picasso.Picasso


class ProfileFragment : Fragment() {

    private val vm : UserViewModel by activityViewModels()
    private var _binding: FragmentProfileBinding? = null
    lateinit var toggle: ActionBarDrawerToggle
    private var localUser: UserEntity? = null

    private val CLIENT_ID = "3d6351c75d652d2"
    private var imageUri: Uri? = null
    lateinit var imagePicker: ImagePicker
    private lateinit var selectedImage: Bitmap

    private lateinit var profileImg : ImageView
    private lateinit var profileName : TextView
    private lateinit var userName : TextView

    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        val root: View = binding.root
        val viewInflater = requireContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val myRoot = LinearLayout(context)
        val itemView: View = inflater.inflate(R.layout.header_navigation_drawer, myRoot)

        profileImg = itemView.findViewById(R.id.profile_image)
        profileName = itemView.findViewById(R.id.profile_name)
        userName = itemView.findViewById(R.id.profile_username)

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.viewPager.adapter = PagerAdapter(childFragmentManager)
        binding.tabLayout.setupWithViewPager(binding.viewPager)
        imagePicker = ImagePicker(requireActivity())

        // load user
        val username = loadUserNameCredentials()
        vm.getProfileByUsername(username).observe(viewLifecycleOwner) { profile ->
            if( profile != null )
                setProfileViewContent(profile)
            else {
                Toast.makeText(context,"Crea tu perfil de Escritor!",Toast.LENGTH_LONG).show()
                val action = ProfileFragmentDirections
                    .actionProfileFragmentToEditableProfileFragment(EditableProfileFragment.CREATE)
                findNavController().navigate(action)
            }
        }

        //Config Profile
        toggle = ActionBarDrawerToggle(requireActivity(), binding.drawerLayout, R.string.open, R.string.close)
        binding.drawerLayout.addDrawerListener(toggle)
        binding.drawerLayout.setScrimColor(resources.getColor(R.color.transparent))
        toggle.syncState()

        (requireActivity() as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.configProfileButton.setOnClickListener {
            binding.drawerLayout.openDrawer(Gravity.RIGHT) // WARNING: Switching RIGHT to END can cause conflicts when menu is displayed.

        }
        binding.navigationView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.contact -> Toast.makeText(context, "lepus.softwareinc@gmail.com", Toast.LENGTH_SHORT).show()
                R.id.logout -> signout()
                R.id.edit_profile -> findNavController().navigate(ProfileFragmentDirections.actionProfileFragmentToEditableProfileFragment(EditableProfileFragment.UPDATE))
                R.id.exit -> binding.drawerLayout.closeDrawer(Gravity.RIGHT)
            }
            binding.drawerLayout.closeDrawer(Gravity.RIGHT)
            true
        }

        binding.profileImg.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                imagePicker.askPermissions()
            } else {
                val intent = Intent(Intent.ACTION_GET_CONTENT)
                intent.type = "image/*"
                startActivityForResult(intent, 100)
            }
        }
    }

    private fun signout() {
        Toast.makeText(context, "Cerrando Sesión...", Toast.LENGTH_SHORT).show()
        // clean Shard preferences
        fun onSuccess() {
            val preferences = activity?.getSharedPreferences("UserCredentials",  Context.MODE_PRIVATE)
            preferences?.edit()?.clear()?.apply() // aply() replace commit()
            startActivity(Intent(context, LoginActivity::class.java))
        }
      // sign out from api
        vm.logout(::onSuccess)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item))
            return true

        return super.onOptionsItemSelected(item)
    }

    private fun setProfileViewContent(profile: Profile) {
        binding.userName.text = profile.profile_name
        binding.signture.text = profile.signature
        binding.description.text = profile.description
        binding.followers.text = profile.followers.toString().plus(resources.getString(R.string.seguidores))
        binding.following.text = profile.following.toString().plus(resources.getString(R.string.following))
        Picasso.get().load(profile.image).into(binding.profileImg)

        Picasso.get().load(profile.image).into(profileImg)
        profileName.text = profile.profile_name
        userName.text = profile.user
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun loadUserNameCredentials(): String {
        val sharedPreferences =
            activity?.getSharedPreferences("UserCredentials", Context.MODE_PRIVATE)
        return sharedPreferences?.getString("EMAIL", null)!!
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // CHANGE CODE FOR GET DIFFERENT REQUESTS
        if (requestCode == 100) { // Upload From Gallery
            imageUri = data?.data!!
            //binding.imageTaken.setImageURI(imageUri) // handle chosen image
            Picasso.get().load(imageUri).centerCrop().fit().into(binding.profileImg)

            val bitmap = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                ImageDecoder.decodeBitmap(ImageDecoder.createSource(requireContext().contentResolver, imageUri!!))
            } else {
                MediaStore.Images.Media.getBitmap(requireContext().contentResolver, imageUri)
            }
            val imgLink = vm.postImgurAndGetLink(bitmap)
            imgLink.observe(viewLifecycleOwner){imageLink ->
                // update local profile
                var updater = vm.liveLocalProfile.value
                updater!!.image = imageLink
                vm.liveLocalProfile.postValue(updater)
                // update api profile
                vm.updateProfile(updater)
            }
        } else if (requestCode == 200) { // From Camera
            var bitmap: Bitmap? = (data?.extras?.get("data") as Bitmap?)
            imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context?.contentResolver, bitmap, "ImageTitle", null))
            if (bitmap != null) {
                binding.profileImg.setImageBitmap(bitmap)
                val imgLink = vm.postImgurAndGetLink(bitmap)
                imgLink.observe(viewLifecycleOwner){imageLink ->
                    // update local profile
                    var updater = vm.liveLocalProfile.value
                    updater!!.image = imageLink
                    vm.liveLocalProfile.postValue(updater)
                    // update api profile
                    vm.updateProfile(updater)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        imagePicker.onRequestPermissionsResult(requestCode, grantResults)
    }
}