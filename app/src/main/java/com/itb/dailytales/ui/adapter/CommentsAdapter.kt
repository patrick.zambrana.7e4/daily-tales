package com.itb.dailytales.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.findViewTreeLifecycleOwner
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.itb.dailytales.R
import com.itb.dailytales.model.CommentResponse
import com.itb.dailytales.model.ReplyResponse
import com.itb.dailytales.ui.viewModel.CommentsViewModel
import com.itb.dailytales.ui.viewModel.UserViewModel
import com.squareup.picasso.Picasso
import java.util.*

class CommentsAdapter() : RecyclerView.Adapter<CommentsAdapter.CommentsViewHolder>() {
    var comments = mutableListOf<CommentResponse>()
    lateinit var userVm: UserViewModel
    lateinit var commentVm: CommentsViewModel
    var lifeCycle: LifecycleOwner? = null
    var auxComment: CommentResponse? = null
    var index = 0
    //private val adapter = RepliesAdapter()

    fun setTaleComments(commentList: List<CommentResponse>) {
        comments = commentList.toMutableList()
        notifyDataSetChanged()
    }

//    fun addCommentReply(commentId: Long, comment: Comment) {
//        Log.i("TEST", "${commentId}")
//        comments.find { it.id == commentId }?.replies?.add(comment)
//        notifyDataSetChanged()
//    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentsViewHolder {
        lifeCycle = parent.findViewTreeLifecycleOwner()!!
        val view = LayoutInflater.from(parent.context).inflate(R.layout.comment_item, parent, false)
        return CommentsViewHolder(view)
    }

    override fun onBindViewHolder(holder: CommentsViewHolder, position: Int) {
        holder.bindData(comments[position])
    }

    override fun getItemCount(): Int {
        return comments.size
    }

    inner class CommentsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var profilePhoto: ImageView = itemView.findViewById(R.id.user_photo)
        private var username: TextView = itemView.findViewById(R.id.username_item)
        private var content: TextView = itemView.findViewById(R.id.content)
        private var time: TextView = itemView.findViewById(R.id.time_ago)
        private var reply: TextView = itemView.findViewById(R.id.reply)
        private var showReplies: TextView = itemView.findViewById(R.id.show_replies)
        private var replies: ConstraintLayout = itemView.findViewById(R.id.replies_body)
        private val adapter = RepliesAdapter()
        private var commentReplies : RecyclerView = itemView.findViewById(R.id.comment_replies)
        private var showRepliesClicked = false

        fun bindData(comment: CommentResponse) {
            Picasso.get().load(comment.profile_id.image).into(profilePhoto)
            username.text = comment.profile_id.profile_name
            content.text = comment.content
            time.text = getTimeAgo(comment.created_at!!)

            commentReplies.layoutManager = LinearLayoutManager(itemView.context)
            adapter.userVm = userVm
            adapter.lifeCycle = lifeCycle!!
            commentReplies.adapter = adapter
                if (comment.replies.isNullOrEmpty())
                    showReplies.visibility = View.GONE
                else {
                    showReplies.text = String.format("Ver respuestas (%d)", comment.replies!!.size)
                    Log.i("REPLIES:", "${comment.replies}")
                    adapter.setCommentReplies(comment.replies as List<ReplyResponse>)
                    index++
                    if (!showRepliesClicked)
                        showReplies.visibility = View.VISIBLE
                }


            showReplies.setOnClickListener {
                auxComment = comment
                showReplies()
                showRepliesClicked = true
            }
            reply.setOnClickListener {
                userVm.liveProfile.observe(lifeCycle!!){ profile -> CommentsViewModel.ReplyTo.userToReply = profile!!}
                CommentsViewModel.ReplyTo.isReplying.postValue(true)
                CommentsViewModel.ReplyTo.commentToReply = comment
            }
        }

        fun showReplies() {
            replies.translationY = -60F
            replies.alpha = 0F
            replies.visibility = View.VISIBLE
            replies.animate().translationY(0F).alpha(1F).setDuration(100).setStartDelay(0).start()
            showReplies.visibility = View.GONE
        }

        fun getTimeAgo(postDate: Date) : String {
            val currentDate = Calendar.getInstance().time.time
            val diff: Long = currentDate - postDate.time //2022-05-09T15:09:04.779478Z
            val seconds = diff / 1000
            val minutes = seconds / 60
            val hours = minutes / 60
            val days = hours / 24
            val weeks = days / 7
            val months = weeks / 4

            return when {
                seconds < 60 -> seconds.toString().plus("s")
                minutes < 60 -> minutes.toString().plus("min")
                hours < 24 -> minutes.toString().plus("h")
                days < 7 -> days.toString().plus("d")
                weeks < 4 -> weeks.toString().plus("sem")
                else -> months.toString().plus("m")
            }
        }
    }
}