package com.itb.dailytales.ui.view

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.itb.dailytales.R
import com.itb.dailytales.databinding.ActivityCreatorBinding
import com.itb.dailytales.model.Tale
import com.itb.dailytales.ui.viewModel.TalesViewModel
import com.itb.dailytales.ui.viewModel.UserViewModel

class CreatorActivity : AppCompatActivity() {
    val taleVm: TalesViewModel by viewModels()
    val userVm : UserViewModel by viewModels()
    private lateinit var binding: ActivityCreatorBinding

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.CustomBackground)
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        supportActionBar?.hide()

        binding = ActivityCreatorBinding.inflate(layoutInflater)
        setContentView(binding.root)
        // listners
        binding.backButton.setOnClickListener {
            finish()
            overridePendingTransition(R.anim.from_top, R.anim.to_bottom)
        }

        // Set custom font to Edit FText's
        var tfRegular = resources.getFont(R.font.montserrat_regular)
        var tfLight = resources.getFont(R.font.montserrat_light)
        binding.creatorTitle.typeface = tfRegular
        binding.creatorTale.typeface = tfLight

        binding.creatorButton.setOnClickListener {
            // bindData and post it
            if(userVm.liveLocalProfile.value == null) {
                val userName = loadUserNameCredentials()
                userVm.getProfileByUsername(userName).observe(this){ profile ->
                    val tale = Tale(0,profile!!.id,binding.creatorTitle.text.toString(), binding.creatorTale.text.toString())
                    taleVm.postTale(tale).observe(this){finish() /* finish activivty*/}
                }
            } else {
                val tale = Tale(
                    0,
                    userVm.liveLocalProfile.value!!.id,
                    binding.creatorTitle.text.toString(),
                    binding.creatorTale.text.toString()
                )
                taleVm.postTale(tale).observe(this){finish() /* finish activivty*/}
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        overridePendingTransition(R.anim.from_top, R.anim.to_bottom) // ANIMATION
    }

    private fun loadUserNameCredentials(): String {
        val sharedPreferences =
            getSharedPreferences("UserCredentials", Context.MODE_PRIVATE)
        return sharedPreferences?.getString("EMAIL", null)!!
    }
}