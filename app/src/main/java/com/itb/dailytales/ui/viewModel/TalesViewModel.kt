package com.itb.dailytales.ui.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.itb.dailytales.model.Tale
import com.itb.dailytales.repository.MainRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class TalesViewModel : ViewModel() {
    // vars
    val repository = MainRepository.getInstance()
    var testTale = Tale(0, 1,"La última cena",
        "El conde me ha invitado a su castillo. Naturalmente yo llevaré la bebida.")
    var liveMyTales = MutableLiveData<List<Tale>>()
    private var isRequestSuccessful =  MutableLiveData<Boolean>()

    companion object {
        var liveTaleList = MutableLiveData<List<Tale>>()
    }

    init {
        getAllTales()
    }
    // api methods
    fun postTale(tale: Tale): MutableLiveData<Boolean>{
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.postTale(tale)}
            if( response.isSuccessful ) {
                Log.i("POST","Tale added succesfully")
                isRequestSuccessful.postValue( response.isSuccessful)
            }
            else {
                Log.i("commentVm",response.body().toString())
                Log.i("commentVm:error -> ",response.message())
            }
        }
        return isRequestSuccessful
    }
    // methods
    fun getAllTales() {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.getTalesByProfileId()}
            if( response.isSuccessful ) {
                liveTaleList.postValue(response.body()!!.shuffled())
            } else {
                Log.e("GET-TALES", "No se ha podido cargar los cuentos")
            }
        }
    }

    fun getTalesByUser(id:Int): MutableLiveData<List<Tale>> {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.getTalesByProfileId(id)}
            if( response.isSuccessful ) {
                liveMyTales.postValue(response.body())
            } else {
                Log.e("Data Retrieve Error", "No se ha podido cargar los cuentos")
            }
        }
        return liveMyTales
    }
}