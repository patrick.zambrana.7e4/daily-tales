package com.itb.dailytales.ui.view

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.itb.dailytales.databinding.FragmentHomeBinding
import com.itb.dailytales.ui.adapter.ScreenSlidePagerAdapter
import com.itb.dailytales.ui.viewModel.TalesViewModel
import com.itb.dailytales.ui.viewModel.UserViewModel

class HomeFragment : Fragment() {

    private val vmUser: UserViewModel by viewModels()
    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = ScreenSlidePagerAdapter(vmUser)
        binding.taleViewpager.adapter = adapter
        TalesViewModel.liveTaleList.observe(viewLifecycleOwner) {
            adapter.loadNextTales(it)
        }

        requireActivity()
            .onBackPressedDispatcher
            .addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    Log.d(TAG, "Fragment back pressed invoked")
                    if (binding.taleViewpager.currentItem == 0)
                        requireActivity().onBackPressed()
                    else
                        binding.taleViewpager.currentItem = binding.taleViewpager.currentItem -1

                    // if you want onBackPressed() to be called as normal afterwards
                    if (isEnabled) {
                        isEnabled = false
                        requireActivity().onBackPressed()
                    }
                }
            }
            )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

/*    private inner class ScreenSlidePagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {
        override fun getItemCount(): Int = 10

        override fun createFragment(position: Int): Fragment = TaleSlidePagerFragment()
    }*/
}




/*
    // TALE DATA
    binding.taleTitle.text = viewModel.testTale.title
    binding.taleText.text = viewModel.testTale.text
    binding.taleSignature.text = viewModel.testTale.signature

    // BUTTONS
    var shake = AnimationUtils.loadAnimation(context, R.anim.shake) // Shake animation

    binding.commentButton.setOnClickListener {
        it.startAnimation(shake)
        NavHostFragment.findNavController(this).navigate(R.id.action_homeFragment_to_commentsActivity) // Navigate to comment section
    }
    binding.shareButton.setOnClickListener {
        it.startAnimation(shake)
        share()
    }
    binding.favButton.setOnLikeListener(object : OnLikeListener {
        override fun liked(likeButton: LikeButton) {
            //TODO
        }
        override fun unLiked(likeButton: LikeButton) {
            //TODO
        }
    })

    *//**
 * Opens menu to share the URL.
 *//*
    private fun share() {
        var intent = Intent(Intent.ACTION_SEND)
        intent.setType("text/plain")
        var shareBody = "Tale URL"
        var shareSub = binding.taleTitle.text
        intent.putExtra(Intent.EXTRA_SUBJECT, shareSub)
        intent.putExtra(Intent.EXTRA_TEXT, shareBody)
        startActivity(Intent.createChooser(intent, "Enviar a"))
    }
*/