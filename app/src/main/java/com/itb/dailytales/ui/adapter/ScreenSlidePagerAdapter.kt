package com.itb.dailytales.ui.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.findViewTreeLifecycleOwner
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.itb.dailytales.R
import com.itb.dailytales.model.Tale
import com.itb.dailytales.ui.view.HomeFragmentDirections
import com.itb.dailytales.ui.viewModel.UserViewModel
import com.like.LikeButton
import com.like.OnLikeListener
import java.lang.StringBuilder


class ScreenSlidePagerAdapter(val vmUser: UserViewModel) : RecyclerView.Adapter<ScreenSlidePagerAdapter.ScreenSlideViewHolder>() {
    var currentLoadedTales = mutableListOf<Tale>()
    var lifeCycle: LifecycleOwner? = null

    fun loadNextTales(taleList: List<Tale>) {
        currentLoadedTales = taleList.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScreenSlideViewHolder {
        lifeCycle = parent.findViewTreeLifecycleOwner()!!
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_pager_tale, parent, false)
        return ScreenSlideViewHolder(view)
    }

    override fun onBindViewHolder(holder: ScreenSlideViewHolder, position: Int) = holder.bindData(currentLoadedTales[position])

    override fun getItemCount(): Int = currentLoadedTales.size

    inner class ScreenSlideViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView = itemView.findViewById(R.id.tale_title)
        var content: TextView = itemView.findViewById(R.id.tale_text)
        var signature: TextView = itemView.findViewById(R.id.tale_signature)

        var favBtn: LikeButton = itemView.findViewById(R.id.fav_button)
        var commentBtn: ImageButton = itemView.findViewById(R.id.comment_button)
        var shareBtn: ImageButton = itemView.findViewById(R.id.share_button)
        val creatorUsername: TextView = itemView.findViewById(R.id.tale_creator_username)

        fun bindData(tale: Tale) {
            title.text = tale.title
            content.text = tale.text
            vmUser.getProfileById(tale.profile_id).observe(lifeCycle!!){
                    profile -> signature.text = profile?.signature ?: ""
                    creatorUsername.text = String.format("%s%n@%s", vmUser.liveProfile.value!!.profile_name, vmUser.liveProfile.value!!.user)
            }

            favBtn.setOnLikeListener(object : OnLikeListener {
                override fun liked(likeButton: LikeButton) {
                    //TODO: When liked
                }
                override fun unLiked(likeButton: LikeButton) {
                    //TODO: When unliked
                }
            })
            commentBtn.setOnClickListener {
                val action = HomeFragmentDirections.actionHomeFragmentToCommentsActivity(tale.id)
                itemView.findNavController().navigate(action)
            }
            shareBtn.setOnClickListener {
                val playStoreLink = "play.google.com/store/apps/details?id=com.itb.dailytales"
                val builder = StringBuilder()
                val taleText = builder.append(tale.title).append("\n\n").append(tale.text ).append("\n\n@")
                    .append(vmUser.liveProfile.value!!.user).append("\n\n").append(playStoreLink).toString()
                share( taleText, tale.title) //TODO: Get Tale URL from API/ViewModel and put it here.
            }
        }

        private fun share(url: String, title: String) {
            var intent = Intent(Intent.ACTION_SEND)
            intent.setType("text/plain")
            var shareBody = url
            var shareSub = title
            intent.putExtra(Intent.EXTRA_SUBJECT, shareSub)
            intent.putExtra(Intent.EXTRA_TEXT, shareBody)
            itemView.context.startActivity(Intent.createChooser(intent, "Enviar a"))
        }
    }
}