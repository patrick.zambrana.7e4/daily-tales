package com.itb.dailytales.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.itb.dailytales.R
import com.itb.dailytales.model.ReplyResponse
import com.itb.dailytales.ui.viewModel.UserViewModel
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.*

class RepliesAdapter : RecyclerView.Adapter<RepliesAdapter.RepliesViewHolder>() {
    var replies = mutableListOf<ReplyResponse>()
    lateinit var userVm: UserViewModel
    lateinit var lifeCycle: LifecycleOwner

    fun setCommentReplies(repliesList: List<ReplyResponse>) {
        replies = repliesList.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepliesAdapter.RepliesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.reply_item, parent, false)
        return RepliesViewHolder(view)
    }

    override fun onBindViewHolder(holder: RepliesAdapter.RepliesViewHolder, position: Int) {
        holder.bindData(replies[position])
    }

    override fun getItemCount(): Int {
        return replies.size
    }

    inner class RepliesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var profilePhoto: ImageView = itemView.findViewById(R.id.user_photo_reply)
        private var username: TextView = itemView.findViewById(R.id.username_item_reply)
        private var content: TextView = itemView.findViewById(R.id.content_reply)
        private var time: TextView = itemView.findViewById(R.id.time_ago_reply)
        private var reply: TextView = itemView.findViewById(R.id.reply_reply)

        fun bindData(reply: ReplyResponse) {
            Picasso.get().load(reply.profile_id.image).into(profilePhoto)
            username.text = reply.profile_id?.profile_name
            content.text = reply.content
            time.text = getTimeAgo(reply.created_at!!)
        }

        fun getTimeAgo(postDate: Date) : String {
            val diff: Long = SimpleDateFormat("dd/M/yyyy hh:mm:ss").calendar.time.time - postDate.time
            val seconds = diff / 1000
            val minutes = seconds / 60
            val hours = minutes / 60
            val days = hours / 24
            val weeks = days / 7
            val months = weeks / 4

            return when {
                seconds < 60 -> seconds.toString().plus("s")
                minutes < 60 -> minutes.toString().plus("min")
                hours < 24 -> minutes.toString().plus("h")
                days < 7 -> days.toString().plus("d")
                weeks < 4 -> weeks.toString().plus("sem")
                else -> months.toString().plus("m")
            }
        }
    }
}