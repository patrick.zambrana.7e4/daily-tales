package com.itb.dailytales.ui.view

import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.itb.dailytales.MainActivity
import com.itb.dailytales.databinding.RegisterTabFragmentBinding
import com.itb.dailytales.model.Register
import com.itb.dailytales.ui.viewModel.UserViewModel

class RegisterTabFragment: Fragment() {
    private val vm : UserViewModel by activityViewModels()
    private var _binding: RegisterTabFragmentBinding? = null

    private val binding get() = _binding!!
    private var lastClickTime: Long = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = RegisterTabFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // listeners
        binding.registerButton.setOnClickListener {
            // preventing double, using threshold of 2000 ms
            if (SystemClock.elapsedRealtime() - lastClickTime > 2000){

                val email = binding.editText.text.toString()
                val username = binding.registerSetUsername.text.toString()
                val password = binding.registerSetPassword.text.toString()
                val confirmPassword = binding.registerConfirmPassword.text.toString()

                if( password == confirmPassword ) {
                    fun onSuccess(response: Register) {
                        startActivity(Intent(context, MainActivity::class.java))
                    }

                    fun onFailure(response: String) {
                        Toast.makeText(
                            context, response,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    vm.register(email, username, password, ::onSuccess, ::onFailure)
                } else {
                    Toast.makeText(
                        context, "Las contraseñas no coinciden...",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            lastClickTime = SystemClock.elapsedRealtime()
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}