package com.itb.dailytales.network

import com.itb.dailytales.model.*
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ApiInterface {
   companion object {
       val BASE_URL = "https://dtales.herokuapp.com/"
       fun create(): ApiInterface {
           val client = OkHttpClient.Builder().build()
           val retrofit = Retrofit.Builder()
               .baseUrl(BASE_URL)
              .addConverterFactory(GsonConverterFactory.create())
               .client(client)
               .build()
           return retrofit.create(ApiInterface::class.java)
       }

       fun getInstance(): ApiInterface {
           return create()
       }
   }
    // api functions
    @GET("users")
    suspend fun getAllUsers(): Response<List<User>>

    // tales
    @GET("tales")
    suspend fun getAllTales(): Response<List<Tale>>

    @GET("tales")
    suspend fun getTalesByProfileId(@Query("profile_id") id: Int): Response<List<Tale>>

    @POST("tales/")
    suspend fun postTale(@Body tale: Tale) : Response<Tale>

    @PUT("tales/{id}/")
    suspend fun putTale(@Body tale:Tale,@Path("id") id: Int): Response<Tale>

    @GET("profiles")
    suspend fun getAllProfiles(): Response<List<Profile>>

    @GET("profiles/")
    suspend fun getProfileByUserName(@Query("user") username: String) : Response<List<Profile>>

    @GET("profiles/")
    suspend fun getProfileById(@Query("id") id: Int) : Response<List<Profile>>

    @PUT("profiles/{id}/")
    suspend fun putProfile(@Body profile:Profile,@Path("id") id: Int): Response<Profile>

    @POST("profiles/")
    suspend fun postProfile(@Body profile: Profile): Response<Profile>

    @POST("auth/login/")
    suspend fun postLogin(@Body login:Login): Response<LoginResponse>

    @POST("auth/signup/")
    suspend fun postRegister(@Body register:Register): Response<Register>

    @POST("auth/logout/")
    suspend fun postLogout(@Body empty: String): Response<Void>

    @GET("comments/")
    suspend fun getCommentsByTaleId(@Query("tale_id") taleId: Int) : Response<List<CommentResponse>>

    @POST("comments/")
    suspend fun postComment(@Body comment: Comment): Response<CommentResponse>

    @PUT("comments/{id}/")
    suspend fun putComment(@Body comment:Comment,@Path("id") id: Int): Response<Comment>

    @GET("replies/")
    suspend fun getRepliesByCommentId(@Query("comment_id") commentId: Int) : Response<List<ReplyResponse>>

    @POST("replies/")
    suspend fun postReply(@Body reply: Reply): Response<ReplyResponse>

    @PUT("replies/{id}/")
    suspend fun putReply(@Body reply:Reply,@Path("id") id: Int): Response<Reply>



}
